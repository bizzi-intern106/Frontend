# PROJECT SCREEN

### 1. SIGNUP
![Alt text](public/system/signup.png)

### 2. LOGIN
![Alt text](public/system/login.png)

### 3. LANDING PAGE
##### 3.1 User (can only view their own email)
![Alt text](public/system/user-landing-page.png)

##### 3.2 Admin (can list of users and their emails)
![Alt text](public/system/admin-landing-page.png)

### 4. EMAIL LIST
![Alt text](public/system/email-list.png)

### 5. EMAIL DETAIL
![Alt text](public/system/email-detail.png)

### 6. INVOICE DETAIL
![Alt text](public/system/invoice-detail.png)
