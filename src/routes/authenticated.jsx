import Sidebar from "../components/sidebar/Sidebar";
import Header from "../components/header/header";
import { Outlet } from "react-router-dom";

export function AuthenticatedLayout() {
    return (
        <>
            <Header />
            <div className="page-wrapper">
                <div className="content mt-5">

                    <Outlet />
                </div>
            </div>
            <Sidebar />
        </>
    );
}