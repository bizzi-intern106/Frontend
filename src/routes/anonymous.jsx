import { Outlet } from "react-router-dom";

export function AnonymousLayout() {
    return (
        <>
            <div id="anonymous">
                <Outlet />
            </div>
            {/* <Footer /> */}
        </>
    );
}