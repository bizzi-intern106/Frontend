import axios from "axios";

export const devAxios = axios.create({
    baseURL: import.meta.env.VITE_AUTH_API
})

export const hasuraAxios = axios.create({
    baseURL: import.meta.env.VITE_GRAPHQL_API,
    headers: {
        "x-hasura-admin-secret": import.meta.env.VITE_GRAPHQL_SECRET
    }
})