import { hasuraAxios } from "../libs/axios";

export const getUsers = async () => {
    const accessToken = localStorage.getItem("accessToken");
    const query = `#graphql
        query MyQuery {
            user {
                id
                role
                username
            }
        }
    `
    const res = await hasuraAxios.post("/graphql", {
        query: query
    }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })

    const users = res.data?.data.user;
    return users || [];
}

export const getUser = async (userId) => {
    const accessToken = localStorage.getItem("accessToken");
    const _query = '"' + userId + '"';
    const query = `
        query MyQuery {
            user(where: {id: {_eq: ${_query}}}) {
                role
                username
                id
            }
        }
    `
    const res = await hasuraAxios.post("/graphql", {
        query: query
    }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })

    const user = res.data?.data.user[0];
    return user || null;
}

export const getUserEmail = async ({ userId }) => {
    const accessToken = localStorage.getItem("accessToken");
    const _query = '"' + userId + '"';
    const query = `
        query MyQuery {
            email(where: {user_id: {_eq: ${_query}}}) {
                id
                content
                subject
                to
                from
            }
        }
    `

    console.log(query);
    const res = await hasuraAxios.post("/graphql", {
        query: query
    }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })

    console.log(res.data);

    const emails = res.data?.data.email;
    return emails || [];
}

export const getEmailDetail = async ({ emailId }) => {
    const accessToken = localStorage.getItem("accessToken");
    const query = `
        query MyQuery {
            email(where: {id: {_eq: "${emailId}"}}) {
                id
                content
                subject
                to
                from
                attachments {
                    id
                    file_name
                    url
                }
                invoices {
                    serial
                    form
                    created_at
                    id
                    full_id
                }
            }
        }
    `
    const res = await hasuraAxios.post("/graphql", {
        query: query
    }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })

    console.log(res.data)

    const email = res.data?.data.email[0];
    console.log(email);
    return email || {};
}

export const getInvoiceDetail = async ({ invoiceId }) => {
    console.log("invoiceId", invoiceId)
    const accessToken = localStorage.getItem("accessToken");
    const _query = '"' + invoiceId + '"';
    const query = `
        query MyQuery {
            invoice(where: {full_id: {_eq: ${_query}}}) {
                currency
                full_id
                serial
                form
                id
                created_at
                exchange_rate
                name
                services {
                    index
                    money
                    name
                    total_money
                    vat_amount
                    vat_rate
                }
                seller {
                    address
                    name
                    tax_code
                    telephone
                }
                buyer {
                    address
                    contract_id
                    end_date
                    name
                    payment_method
                    start_date
                    tax_code
                }
                payment {
                    tax_rate
                    total
                    total_char
                    total_money_before_tax
                    total_tax_money
                }
            }
        }

    `
    console.log(query);
    const res = await hasuraAxios.post("/graphql", {
        query: query
    }, {
        headers: {
            Authorization: `Bearer ${accessToken}`
        }
    })

    const invoice = res.data?.data.invoice[0];
    console.log(res.data);
    return invoice || {};
}