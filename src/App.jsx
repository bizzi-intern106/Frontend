import './App.css'
import SignUp from './pages/signup'
import Login from './pages/login'
import { Route, BrowserRouter, Routes } from 'react-router-dom'
import ProtectRoute from './components/protect/protectRouter'
import Home from './pages/home'
import { AnonymousLayout } from './routes/anonymous'
import { AuthenticatedLayout } from './routes/authenticated'
import Email from "./pages/email/detail";
import User from "./pages/user/detail";
import Invoice from "./pages/invoice/detail";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<AuthenticatedLayout />}>
          <Route path="/" element={<ProtectRoute />}>
            <Route index element={<Home />} />
            <Route path="/user/:id" element={<User />} />
            <Route path="/invoice/:id" element={<Invoice />} />
            <Route path="/email/:id" element={<Email />} />
          </Route>
        </Route>

        <Route path="/" element={<AnonymousLayout />}>
          <Route path='/signup' element={<SignUp />} />
          <Route path='/login' element={<Login />} />
        </Route>

      </Routes>
    </BrowserRouter>
  )
}

export default App
