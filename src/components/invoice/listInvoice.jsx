import { Link } from "react-router-dom";
import EmptyList from "../empty/emptyList";

export default function ListInvoice({ elements, headers, tableName, route }) {
    return <div className="row">
        <div className="card">
            <div className="card-header">
                <h4 className="card-title">{tableName}</h4>
            </div>
            <div className="card-body">
                <div className="table-responsive">

                    <EmptyList
                        list={elements}>
                        <table className="table mb-0">
                            <thead>
                                <tr>
                                    {
                                        headers && headers.map(header => {
                                            return <th key={header}>{header}</th>
                                        })
                                    }
                                </tr>
                            </thead>
                            <tbody>
                                {
                                    elements && elements.map(user => {
                                        const { form, serial, id, created_at, full_id } = user;
                                        return <tr>
                                            <td>{form}</td>
                                            <td>{serial}</td>
                                            <td>{id}</td>
                                            <td>{created_at} </td>
                                            <td>
                                                <Link to={`/${route}/${full_id}`}> Go to {route} details</Link>
                                            </td>
                                        </tr>
                                    })
                                }
                            </tbody>
                        </table>
                    </EmptyList>
                </div>
            </div>
        </div>
    </div>
}