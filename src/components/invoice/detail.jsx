import "./detail.css";

export default function InvoiceDetail({ invoice }) {
    const {
        services,
        seller,
        buyer,
        payment,
        // hoa don
        serial,
        form,
        id,
        created_at,
    } = invoice;

    console.log(invoice)
    const goods = services && services.map(goods => {
        const { index, name, money, vat_rate, vat_amount } = goods;
        return <tr>
            <td>{index}</td>
            <td>{name}</td>
            <td></td>
            <td></td>
            <td></td>
            <td className="text-right">{money * 10 ** 6}</td>
        </tr>
    })

    const _payment = payment &&
        <>
            <tr className="text-right">
                <td colSpan={5}>Tổng tiền chưa có thuế GTGT</td>
                <td>{payment?.total_money_before_tax * 10 ** 6}</td>
            </tr>
            <tr className="text-right">
                <td colSpan={5}>Tổng tiền thuế GTGT {payment?.tax_rate}%</td>
                <td>{payment?.total_tax_money * 1000} </td>
            </tr>
            <tr className="text-right">
                <td colSpan={5}>Tổng tiền thuế đã có GTGT</td>
                <td ><span className="red bold">{payment?.total * 10 ** 6}</span></td>
            </tr>
            <tr>
                <td colSpan={6}>Tổng tiền bằng chữ <span className="bold">{payment?.total_char}</span></td>
            </tr>
        </>
    return (
        <div className="row">
            <div className="invoice-header">
                <h3>Chi tiết hóa đơn {id}</h3>
            </div>
            <div className="invoice-container">
                <div className="invoice-container-header">
                    <div className="invoice-header-item">
                        <p>Số hóa đơn:</p>
                        <span className="red bold">{id}</span>
                    </div>
                    <div className="invoice-header-item">
                        <p>Ký hiệu:</p>
                        <span className="red bold">{serial}</span>
                    </div>
                    <div className="invoice-header-item">
                        <p>Mẫu số:</p>
                        <span className="red bold">{form}</span>
                    </div>
                    <div className="invoice-header-item">
                        <p>Ngày lập:</p>
                        <span className="red bold">{created_at}</span>
                    </div>
                </div>
                <div className="invoice-container-seller">
                    <p>Đơn vị bán hàng: <span>{seller?.name}</span></p>
                    <p>Mã số thuế: <span>{seller?.tax_code}</span></p>
                    <p>Địa chỉ: <span>{seller?.address}</span></p>
                    <p>Số điện thoại: <span>{seller?.telephone}</span></p>
                </div>
                <div className="invoice-container-buyer">
                    <p>Họ tên người mua: </p>
                    <p>Đơn vị mua hàng: <span>{buyer?.name}</span></p>
                    <p>Mã số thuế: <span>{buyer?.tax_code}</span></p>
                    <p>Địa chỉ: <span>{buyer?.address}</span></p>
                    <p>Hình thức chuyển khoản: <span>{buyer?.payment_method}</span></p>
                </div>

                <div className="invoice-container-goods">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table mb-0">
                                <thead>
                                    <tr>
                                        <th>index</th>
                                        <th>Tên hoàng hóa dịch vụ</th>
                                        <th>Đơn vị tính</th>
                                        <th>Số lượng</th>
                                        <th>Đơn giá</th>
                                        <th>Thành tiền chưa có thuế</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {
                                        goods
                                    }
                                    {
                                        _payment
                                    }
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}