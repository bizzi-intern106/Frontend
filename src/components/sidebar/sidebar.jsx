export default function Sidebar() {
    return (
        <div className="sidebar" id="sidebar">
            <div className="sidebar-inner slimscroll">
                <div id="sidebar-menu" className="sidebar-menu">
                    <ul>
                        <li> <a href="index.html"><i className="fas fa-tachometer-alt"></i> <span>Dashboard</span></a> </li>
                        <li className="list-divider"></li>
                    </ul>
                </div>
            </div>
        </div>
    )
}