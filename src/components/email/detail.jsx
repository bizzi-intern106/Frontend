import ListInvoice from "../invoice/listInvoice";
import parser from "html-react-parser";

export default function EmailDetail({ subject, from, to, attachments, content, invoice }) {
    return (
        <div>
            <div className="row">
                <div className="col-sm-12">
                    <h4 className="page-title">View Message</h4>
                </div>
            </div>
            <div className="row mt-3">
                <div className="col-sm-12">
                    <div className="card-box">
                        <div className="mailview-content">
                            <div className="mailview-header">
                                <div className="row">
                                    <div className="col-sm-9">
                                        <div className="text-ellipsis m-b-10">
                                            <span className="mail-view-title">{subject}</span>
                                        </div>
                                    </div>
                                    <div className="col-sm-3">
                                        <div className="mail-view-action">
                                            <div className="btn-group">
                                                <button type="button" className="btn btn-white btn-sm"
                                                    data-toggle="tooltip" title="Delete"> <i
                                                        className="fa fa-trash-o"></i></button>
                                                <button type="button" className="btn btn-white btn-sm"
                                                    data-toggle="tooltip" title="Reply"> <i
                                                        className="fa fa-reply"></i></button>
                                                <button type="button" className="btn btn-white btn-sm"
                                                    data-toggle="tooltip" title="Forward"> <i
                                                        className="fa fa-share"></i></button>
                                            </div>
                                            <button type="button" className="btn btn-white btn-sm" data-toggle="tooltip"
                                                title="Print"> <i className="fas fa-print"></i></button>
                                        </div>
                                    </div>
                                </div>
                                <div className="sender-info">
                                    <div className="sender-img">
                                        <img width="40" alt="" src="assets/img/profiles/avatar-15.jpg"
                                            className="rounded-circle" />
                                    </div>
                                    <div className="receiver-details float-left">
                                        <span className="sender-name">{from}</span>
                                        <span className="receiver-name">
                                            to <span>{to}</span>
                                        </span>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                            </div>
                            <div className="mailview-inner">
                                {parser(content || "")}
                            </div>
                        </div>
                        {
                            attachments &&
                            <div className="mail-attachments">
                                <p><i className="fas fa-paperclip"></i> {attachments.length} Attachments - <a href="#">View all</a> | <a
                                    href="#">Download all</a></p>
                                <ul className="attachments clearfix text-center">
                                    {attachments.map(file => {
                                        const { file_name, url, id } = file;
                                        return (
                                            <li>
                                                <div className="attach-file"><i className="fas fa-file-alt"></i></div>
                                                <div className="attach-info">
                                                    <div>
                                                        {file_name}
                                                    </div>
                                                    <div>

                                                        <a href={url}>Download file</a>
                                                    </div>
                                                </div>

                                            </li>
                                        )
                                    })}
                                </ul>
                            </div>
                        }
                        <div className="mailview-footer">
                            <ListInvoice
                                elements={invoice}
                                headers={["MSHDon", "KHHDon", "SHDon", "TDLap", "Detail"]}
                                tableName="Invoice"
                                route="invoice" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}