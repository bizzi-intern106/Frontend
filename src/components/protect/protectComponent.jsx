function ProtectComponent({ children, allowRoles = [] }) {
    if (allowRoles.length > 0) {
        authorized = authorized && allowRoles.some(role => role === authState.role);
    }

    return (
        authorized
            ? <>
                {children}
            </>
            : <>
                {/* render nothing */}
            </>
    )
}

export default ProtectComponent;