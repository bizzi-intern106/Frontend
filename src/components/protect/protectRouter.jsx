import { Navigate, Outlet, useLocation } from 'react-router-dom';

// protect route
function ProtectRoute({ allowRoles = [] }) {
    const location = useLocation();
    const accessToken = localStorage.getItem("accessToken");
    let authorized = accessToken ? true : false;
    if (allowRoles.length > 0) {
        authorized = authorized && allowRoles.some(role => role === authState.role);
    }

    return (
        authorized
            ? <Outlet />
            : accessToken // nếu đăng nhập với quyền staff nhưng page quyền quản lý
                ? <Navigate to='/unauthorization' state={{ from: location }} replace />
                : <Navigate to='/login' state={{ from: location }} replace />
    )
}

export default ProtectRoute;