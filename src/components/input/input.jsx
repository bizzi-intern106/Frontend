import "./input.css";

export default function FormInput({ content, handle, icon, placeholder, errorContent, type }) {
    return (
        <>
            <div className="form-input">
                <div className="form-input__icon">
                    <i className={icon}></i>
                </div>
                <input
                    value={content}
                    onBlur={(e) => handle(e)}
                    onChange={(e) => handle(e)}
                    className="form-input__input" placeholder={placeholder} type={type} />
            </div>
            <span className="validate-input">{errorContent}</span>
        </>
    )
}