import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

export default function Header() {
    const navigate = useNavigate();
    const [user, setUser] = useState(null);
    const logout = () => {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("user");
        navigate("/login")
    }

    useEffect(() => {
        const _user = JSON.parse(localStorage.getItem("user"));
        setUser(_user);
    }, []);

    return (
        <div className="header">
            <div className="header-left">
                <a href="index.html" className="logo"> <img src="https://lh3.googleusercontent.com/a/ACg8ocKgY06wxC3jJQXhtKZqhNWAuJlxlXMkuJYMmvhwOSNadw=s360-c-no" width="50" height="70" alt="logo" /> <span className="logoclassName">Bizzi</span> </a>
                <a href="index.html" className="logo logo-small"> <img src="https://lh3.googleusercontent.com/a/ACg8ocKgY06wxC3jJQXhtKZqhNWAuJlxlXMkuJYMmvhwOSNadw=s360-c-no" alt="Logo" width="30" height="30" /> </a>
            </div>
            <a href="javascript:void(0);" id="toggle_btn"> <i className="fe fe-text-align-left"></i> </a>
            <a className="mobile_btn" id="mobile_btn"> <i className="fas fa-bars"></i> </a>
            <ul className="nav user-menu">
                <li className="nav-item dropdown has-arrow">
                    <a href="#" className="dropdown-toggle nav-link" data-toggle="dropdown"> <span className="user-img"><img className="rounded-circle" src="https://lh3.googleusercontent.com/a/ACg8ocKgY06wxC3jJQXhtKZqhNWAuJlxlXMkuJYMmvhwOSNadw=s360-c-no" width="31" alt="Soeng Souy" /></span> </a>
                    <div className="dropdown-menu">
                        <div className="user-header">
                            <div className="avatar avatar-sm"> <img src="https://lh3.googleusercontent.com/a/ACg8ocKgY06wxC3jJQXhtKZqhNWAuJlxlXMkuJYMmvhwOSNadw=s360-c-no" alt="User Image" className="avatar-img rounded-circle" /> </div>
                            <div className="user-text">
                                <h6>{user?.username}</h6>
                                <p className="text-muted mb-0">{user?.role}</p>
                            </div>
                        </div>
                        <a className="dropdown-item" href="profile.html">My Profile</a>
                        <a className="dropdown-item" href="settings.html">Account Settings</a>
                        <div className="dropdown-item" onClick={() => logout()}>Logout</div> </div>
                </li>
            </ul>
        </div>
    )
}