export default function EmptyList({ list, children }) {
    return list && list.length === 0 ?
        <p className="text-center">Danh sách rỗng</p>
        : children
}