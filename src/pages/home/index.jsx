import ListUser from "../../components/user/listUser";
import ListEmail from "../../components/email/listEmail";
import { useState, useEffect } from "react";
import {
    getUsers,
    getUserEmail
} from "../../services/graph-service";

export default function Home() {
    const [elements, setElements] = useState([]);
    const [user, setUser] = useState(null); // can get

    useEffect(() => {
        const _user = localStorage.getItem("user");
        const parse = JSON.parse(_user);
        if (_user) {
            setUser(parse);
        }

        if (parse?.role === "admin") {
            getUsers()
                .then(users => setElements(users));
        } else {
            getUserEmail({ userId: parse?.id || "" })
                .then(emails => setElements(emails))
        }
    }, [])

    const render = user?.role === "admin" ?
        <ListUser
            elements={elements}
            headers={["Id", "Username", "Role", "Detail"]}
            tableName="User"
            route="user" /> :
        <ListEmail
            elements={elements}
            headers={["Id", "From", "To", "Subject", "Detail"]}
            tableName="User email"
            route="email" />

    return (
        <>
            {render}
        </>
    )
}