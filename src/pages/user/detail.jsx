
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import ListEmail from "../../components/email/listEmail";
import { getUserEmail } from "../../services/graph-service";

export default function User() {
    const { id } = useParams();
    const [elements, setElements] = useState([]);

    useEffect(() => {
        getUserEmail({ userId: id || "" })
            .then(emails => setElements(emails))
    }, [])

    return (
        <ListEmail
            elements={elements}
            headers={["Id", "From", "To", "Subject", "Detail"]}
            tableName="User Email"
            route="email" />
    )
}