
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import EmailDetail from "../../components/email/detail";
import { getEmailDetail } from "../../services/graph-service";

export default function Email() {
    const { id } = useParams();
    const [element, setElements] = useState({});

    useEffect(() => {
        getEmailDetail({ emailId: id || "" })
            .then(email => setElements(email))
    }, [])

    return (
        <EmailDetail
            from={element?.from}
            to={element?.to}
            subject={element?.subject}
            content={element?.content}
            attachments={element?.attachments}
            invoice={element?.invoices} />
    )
}