import { Link, useNavigate } from "react-router-dom";
import "./signup.css";
import FormInput from "../../components/input/input";
import FormButton from "../../components/button/button";
import { useState } from "react";
import { signup } from "../../services";
import toastr from "toastr";

export default function SignUp() {
    const navigate = useNavigate();
    const [username, setUsername] = useState({
        content: "",
        error: ""
    });
    const [password, setPassword] = useState({
        content: "",
        error: ""
    });
    const [checkBox, setCheckbox] = useState(false);

    const handleUsernameChange = (e) => {
        const value = e.target.value;
        let error = "";
        // validate username
        if (value === "") {
            error = "username is required";
        }
        setUsername({
            ...username,
            content: value,
            error
        });
    }

    const handlePasswordChange = (e) => {
        const value = e.target.value;
        // validate password
        let error = "";
        if (value === "") {
            error = "password is required";
        }
        setPassword({
            ...password,
            content: value,
            error
        });
    }

    const handleSubmit = () => {
        if (!username.error && username === "") {
            setUsername({
                ...username,
                error: "username is required"
            })
        }

        if (!password.error && password === "") {
            setPassword({
                ...password,
                error: "password is required"
            })
        }

        if (!checkBox) {
            toastr.error("you must agree with our term of service!");
            return;
        }

        signup({ username: username.content, password: password.content })
            .then(() => {
                toastr.success("signup successfully");
                navigate("/login");
            })
            .catch(err => {
                console.log(err.response.data);
                if (err?.response.data) {
                    const { statusCode, message } = err.response.data;
                    toastr.error(message);
                    return;
                }

                toastr.error("internal server error");
            })

    }

    return (
        <div className="wrapper">
            <div className="wrapper-form">
                <div className="signup-side">
                    <div className="signup-site__header">
                        <h3>Sign Up</h3>
                    </div>
                    <div className="signup-side__form">
                        <FormInput
                            type={"text"}
                            content={username.content}
                            errorContent={username.error}
                            handle={handleUsernameChange}
                            icon={"uil uil-user"} placeholder={"username"} />
                        <FormInput
                            type={"password"}
                            content={password.content}
                            errorContent={password.error}
                            handle={handlePasswordChange}
                            icon={"uil uil-key-skeleton"} placeholder={"password"} />
                        <div className="form-confirm">
                            <input
                                checked={checkBox}
                                onChange={() => setCheckbox(!checkBox)}
                                className="form-confirm__input" id="form-confirm__input" type="checkbox" />
                            <label htmlFor="form-confirm__input">I agree all statement in <span>Term of service</span></label>
                        </div>
                    </div>
                </div>
                <div className="signup-description">
                    <div className="signup-description__img">
                        <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAA8FBMVEUUbv////////wAbPpPi+cUb/0Ubf///v8AYfT///r//v0AZPz///f///m61/IUb/z4//wAav84ge7/+/8AX/UAYu8AYfIAZP3///D5/f//+voScfv2//8AYv8AXPIAZ/3X7PkAXv0Rcff//+1vouxIiuopePPj8/Xq+P/T5e2u0vOUvO9il/IhavOpyfKjye04euuWufzF3PKZxvHw//VehPFFi+PV7f0NaeZ8svOCsut/pvTi/v8Lcd5amek/d+03dPUAVPlyoeWj0ezB3PTk5+1KidCtx+qkwfC33+9kmOxrqe7J1e4AUOGryvu60/vrQc47AAALeUlEQVR4nO2djVvauhrAk9QmJKSkBQqUAsUKwlAU3e50zjPnjufOuXu3//+/uSk4BfoJR0fOc/PT+bG1PHl5k/crbzoANBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9FoNK+KAKZEzD83+eObAGO869EXAmNzu9vw/OsLj+YVMDE2gLEVvjDAP0BCYRrVvW0YVg/DRr3eDBwQvU+7liMV0welEdwSBMdvJkfH01LTEQA7/q6FSUaAqb2thJxS1EaWNTo5drvCD8BWK/rVwceIbSkhIZBQSDlDsHM66yqqQ4zPKNpSQoQo9SCyLUsKiSqzMlDR8ojGOd1SwIiF+hmLFiV86/pAYCx2LdMabof/DQmXREWw1Z8ZwBeq6bEK/44OV6jRd/9yfV8xCZ2PL6LCOZxC9B4DxWZpecLtbS1NDDlVO4eOYj7DvbC29hZJ8L6rltfAl3Mr+HIw+KG5a6FWcI7Ry0ro8U64a6GWMZ0jjl7O1EhqNr8CKvnE7l2LUPKCEjKP9rvK+ESZpvc6nLzoLIWEoZlQxifK3BB6BCXpkDG2nYnlHXgFDFU8BvY/po6UcuTVWhQRGVxvyM+pKiqUIv5IFdDrUH59ZjPY2TgiuHB3LdcTZr2fOkxq0Z9/3A47B7WNDdG4rsw6NJz0YdLxp6kjguq7WgdusiJlVmzXQ0XSfSHz+8RBImpZJ9Fikp8379pQ2tuNzE5PGW8RHCVKyBkcv3cXdVQHHI6g3dmsDqCMhKJ5nqiatndacjCeRtf4n7uHI05qmwiozjoU+3IGJjCqdoUpxDzycqSmp30rqscVVuOorooO8Q1cmX3y55rX5lf1xfieR+n2qVR20fiVnZdVSfSdL5AuT1NOPJtffG6u20E/vP3GZABQVIcnAVBEwuArXKkGe6hNv98exgbniLD3zW4VrhwPlKl/10fsaZIiBBlB55eOH/dkQg64N5lfVsD7M1gVTUUkdBG3HkfFqFXj9pd6qqc29v9ENPIjORCvNaqrMUXl5NtDTyEnI20+CUV64irC27eckVaehAhZE2XKGME9fDYe9N3xPjbTYy0swv2zg/xJyjn85PxGITJp3j2mhkx+nMogLcRJq3BBiIXZ+IhYrle0PEUyCx+IhieTXII4ZLy/14yCNJyRuWIMcPMLQryTuRZr8ESRSeoDYxaNiERG9KQnJ5ZhhL6fWSeTWvzUqdVoloiEzn6XCDnIuOpLND/lRP2rejt1G5L9hpF6PXbd0tQNev+2W5kxKq2UFdliE05wMn/Pmcxw+eMGGR+nraH6mFF5EUV2zbPSpIt0y25CNVJD4E/Lb2JGAp6Uk4bnO+HlkhSpk5TxNvxqCEW8vTAMb22oMsD5ECRd62PjOGtmPkKYRceXwFekGizAXmyINqwmLSEsIteZD+M1uudIe/TbhUnGOFv3bMT26kmjk56we57nBqP5QPhRoM4ecBjl92shCq8kFzpD4HbyFBgZq/a33nYtZK+C2buOqQUdJfepCVDNE1BmXhx9uw0V2nfCMxQ3ins42Q4aH3JzCpk+fmsoYmPm4OBjK74xMcVJOsDY+JnTzRC90knPFOlx7e8nmFg2Wx0j6u8nqzCsj1oEsRgeqcnkhNSIRS170JQaVGgZAvcCxaq8X4MUCS8RshPgiHgQ0ShOPZ8aviKe/hF8mVCQGDjJnSLNB8bnTWyrH0SuZE4ognR0XAehSIwWdoYzZKudQpHdOYxcX8LF+Pt/KimMpJjjwaJsoZCdMYHpnHFvRUJmW+N62g1OOZmu9CL063+7MhX7nePPx8S4WTlYDUuRzSubTjN/OrZlRnz9oeco5Acj5vv3azUl5vHksDvjZXon0KsRmVZdRzWe1xnrtuCZDLNXKryE8OqGFaTgwbKj/lm700YTZSqIc3wQru/fM9TqNDZ6DZkzekymxI9zvT9VqIdG5jfO+v49Y/y8tNmL7N8th318pFI/mykS9u/Z0SaTFIvpF74UuiNO7vZfbcAbI0Q5FnVT+LDRSsIzu728NUds66r8WgPeGIGHscyJsLCwhAJg4I5WNzGiMP7SVGUtiuAoliuwvlvc3vtm9xSx2H7ifbf4u/S6iOZdTIf8By58tMc0jfcQxZrFaKuhyMYo8LujWNjNP6ZkvwmYZXdEib1W+2aEDwJFYht8iPiaEgn/XDx5NdyUjLiiyrmZYABjueG4XrgKaOJBK+rhiFc2xj1FLE3wNf72/wyKZj8m/tyR0WhcQvmuKTJJQbMfT3/PMC40TU1s9vrztD5BQjRUxNI0KF/tjaEWHGLTwUtIl5cosGl2j+ZbqgkgVbZ/nSFaa1OTrs2VEi5fZABjmnSzGQ5TK4sIDn6PBHkER+t2hnl/zWbVdT4n3Sxur1P3D6WEahx97lbaa53N1LYZXNsWZGiQNOV6f5LU7i8lJMShKUpelJfnQDwUs60hDvEnntj4vriHPyhQrzGBOIQFJIycW+zm0LnsHNipOiRWVQEJpQ0ZyBlYoJNyEk+G/N4diwfczxJ6UxU8vmH8oChfQgavYoUpc/9K6m894FuScNzYfT3KDEWpX+SsCIGr3ts0HV9UIy9D0tfhXVMBHfrmtFCfKOusukNpokCjn31ECh0FOzel8/y+iICQn5eWZxzGoWjct+zsxrahY+xexKieX0jC+2BlxpnA+YRskjW/WacHFEjyw2alyNEJhB7AahQnHUWWeSKtDqw0Vah8i951scMhN6vZlGhUMnWPpI8dOKYClgYcxrPfJMal1WbT8iC7A5p67dG0WAL2umDwwAtJWAlWGobxzJurMO1Wgmx+WjZUOO8UnKLc03aRsq6M6bNCTNF74+WcQ0Qw3uS/E9wLlqtDD3I6BOKpcOPj4AjmhEHewU81eoPxpZfRXPhrtJYFG+B5XzcMhpBm9s3Ke+ybXcr1jDOkdv4ypORNefrURWka03GUi2TeZ927CuQVDgbBB26ndsD+QtqiH0/N2gLg0s/sJn0iQx17Wryi/HrIwTYnrfW20gQVzhtPFvhT4Dy0SOZREoTabOCr8LyvUKpjzPNP9kjv/WwXHXzZoVammbEJrdQFUKDOJjA+hDbieSIiMnpqPDGBe47aduoZ0vm5fTSaqvGkCCwyzt8vS0gr3cUdpgwRzlKqo49whtp0pkw/FD4pJOFSfi9m79oZAkbtYDX4oET1AkS726X08/fLw17K73ujWuYaZOSAXu0LBaK1CBF2c/z2Lx02fhnG5inhNkz3hcyuobOSH6gh4Hz/vsgDEshFd1HCcKKG/gwVWoRa8H6jPpzXxXSuiuWGXxcVJT+cXmddxr0OhG97iugvQpQL5feQDRbP6zJvM69niBywL8p0J0T4+/H9+8ShHy7y+/J3mJoxEekJEekMm0Zib/huwOZhXoawEHAUFXZltDaj6ce3GaLcOr9RII5ZxvlEC+b3prSlwu2zTqrO7VobXvVSGsN3RnBS7HlBR0GUOblvaUY0U+PfZl1fpTUY0bwo4CxYdLBk6kc7xQgie+2f5gf3oxav/nEvOqWpzhqc01jf3U7A44g3oucLu+PV6qGUqs2IRzknFu8P6spEokvI/J7l76px1K9jDLqT+JM+bJvzmvw2qe4r5AOXCK5yxZtPwvsA4+ABxndRufyb0eS43jV9FTUol+Gklp3JziW0o/ze+GzXnndhfpmb85Pv1VI5erK1Qj5wmboHabQ5mg2Df2C/1GfzXVQGO+Pxm/PKj6PBXlgvN1XYPcui1GiU8ujVL4XM1i+jn+sS+aVcCgIncLLP6qvBwj3jjA95jS/NjCEefwOP303TF6YC1cI8zF9CZuCIwPSxP4/Gov8XQqbNwpdI//EP0GEx1HQEGo1Go9FoNBqNRqPRaDQajUaj0Wg0Go1Go9FoNBqNRqPRaDQajUaj0Wg0/7f8D6Dt1mnqpjJCAAAAAElFTkSuQmCC" alt="" />
                    </div>

                </div>
            </div>

            <div className="signup-footer">
                <div className="form-submit">
                    <FormButton
                        submit={handleSubmit}
                        content={"register"} />
                </div>
                <div className="signup-description__link">
                    <Link to="../login">I am ready member</Link>
                </div>
            </div>
        </div>
    )
}