import { useState } from "react";
import InvoiceDetail from "../../components/invoice/detail";
import { useEffect } from "react";
import { getInvoiceDetail } from "../../services/graph-service";
import { useParams } from "react-router-dom";

export default function Invoice() {
    const { id } = useParams();
    const [invoice, setInvoice] = useState({});
    useEffect(() => {
        console.log(id);
        getInvoiceDetail({ invoiceId: id })
            .then(data => setInvoice(data));
    }, [])
    return <InvoiceDetail
        invoice={invoice}
    />
}