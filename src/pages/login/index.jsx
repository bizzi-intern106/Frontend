import { Link, useNavigate } from "react-router-dom";
import "./login.css";
import FormInput from "../../components/input/input";
import FormButton from "../../components/button/button";
import { useState } from "react";
import { login } from "../../services";
import toastr from "toastr";

export default function Login() {
    const navigate = useNavigate();
    const [username, setUsername] = useState("");
    const [password, setPassword] = useState("");

    const handleUsernameChange = (e) => {
        const value = e.target.value;
        setUsername(value);
    }

    const handlePasswordChange = (e) => {
        const value = e.target.value;
        setPassword(value);
    }

    const handleLogin = () => {
        login({ username, password })
            .then((data) => {
                //handle login 
                const { token, user } = data;
                localStorage.setItem("accessToken", token.accessToken);
                localStorage.setItem("user", JSON.stringify(user));

                navigate("/");
            })
            .catch((err) => {
                if (err?.response.data) {
                    const { statusCode, message } = err.response.data;
                    toastr.error(message);
                    return;
                }

                toastr.error("internal server error");
            })
    }
    return (
        <div className="wrapper">
            <div className="wrapper-form">
                <div className="login-description">
                    <div className="login-description__img">
                        <img src="https://bizzi.vn/wp-content/themes/bizzziii/img/register-img-2.svg" alt="" />
                    </div>
                </div>
                <div className="login-side">
                    <div className="login-site__header">
                        <h3>Login</h3>
                    </div>
                    <div className="login-side__form">
                        <FormInput
                            type={"text"}
                            handle={handleUsernameChange}
                            value={username}
                            icon={"uil uil-user"} placeholder={"username"} />
                        <FormInput
                            type={"password"}
                            value={password}
                            handle={handlePasswordChange}
                            icon={"uil uil-key-skeleton"} placeholder={"password"} />
                        <div className="form-confirm">
                            <input className="form-confirm__input" id="form-confirm__input" type="checkbox" />
                            <label htmlFor="form-confirm__input">Remember password</label>
                        </div>
                    </div>
                    <div className="form-submit">
                        <FormButton
                            submit={handleLogin}
                            content={"Login"} />
                    </div>
                </div>
            </div>

            <div className="login-footer">
                <div className="login-description__link">
                    <Link to="../signup">Create an account</Link>
                </div>
                <div className="login-media">
                    <div className="login-media__content">
                        Or login with <span>
                            <i class="uil uil-facebook"></i>
                            <i class="uil uil-instagram-alt"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    )
}